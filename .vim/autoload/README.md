# autoload

Autoload should contain vim-plug. Use this link and follow the instructions:
https://github.com/junegunn/vim-plug

Don't forget to PlugInstall to get the plugins installed and PlugUpdate to update plugins
