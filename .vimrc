" defaults are good
source $VIMRUNTIME/defaults.vim
" add numbers to the side
set number
" tabs are 4 spaces wide (but are real tabs)
set shiftwidth=4
set tabstop=4


" set colours
" https://github.com/crusoexia/vim-monokai
colorscheme monokai

" manage plugins:
" https://github.com/junegunn/vim-plug
call plug#begin()
Plug 'https://github.com/natebosch/vim-lsc.git'
Plug 'https://github.com/junegunn/fzf.git'
Plug 'https://github.com/junegunn/fzf.vim.git'
call plug#end()

" lsc:

"" keybindings:
let g:lsc_auto_map = {
	\ 'GoToDefinition': '<C-]>',
	\ 'GoToDefinitionSplit': ['<C-W>]', '<C-W><C-]>'],
	\ 'FindReferences': 'gr',
	\ 'NextReference': '<C-n>',
	\ 'PreviousReference': '<C-p>',
	\ 'FindImplementations': 'gI',
	\ 'FindCodeActions': 'ga',
	\ 'Rename': 'gR',
	\ 'ShowHover': v:true,
	\ 'DocumentSymbol': 'go',
	\ 'WorkspaceSymbol': 'gS',
	\ 'SignatureHelp': 'gm',
	\ 'Completion': 'completefunc',
	\}

"" commands to use
let g:lsc_server_commands = {
	\ 'go': 'gopls',	
	\ 'c': 'ccls-config',
	\ 'h': 'ccls-config',
	\ 'sh': 'bash-language-server start',
	\ 'bash': 'bash-language-server start',
	\}

" example
"  let g:lsc_server_commands = {
"     \ 'dart': 'dart_language_server',
"     \ 'html': 'dart_language_server',
"     \ 'typescript': 'localhost:8000',
"     \ 'lua': {
"     \   'name': 'lua',
"     \   'command': 'lua-ls',
"     \   'enabled': v:false,
"     \ },
"     \}

" FZF
let g:fzf_action = { 'enter': 'tabnew' }
"" Keybindings
nmap <C-f> :Files<CR>
imap <C-f> <esc>:Files<CR>

" Vanilla stuff
"" On write actions
autocmd BufWritePost *.go !goimports -l -w *.go
