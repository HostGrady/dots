# Dots:
Contains dot files I felt comfortable sharing with the public. 

This git repository is designed to be git cloned into your home directory on a fresh install but it
could probably also be used to copy indvidual files if you just want 1 or 2 important files

## Motivation:
Essentially: I got sick of constantly having to reconfigure everything when I distro hopped.

## License:
Unlicense, public domain. 

If you think I "stole" your code or config file and want credit, make an issue and I will credit you.

## Required Programs:
Here is a non-exhaustive list of programs you will need in order for HostGrady/dots to be compatible (WIP):
Note that these are the names in ArchLinux, they may be differnt for your system

- various standard utilities (vim, X11, coreutils, locate, find, etc)
- entr
- dunst
- tint2
- openbox
- alacritty
- shfmt
- ccls
- gopls
- bash-language-server
- exa
- font-awesome
- go-tools (for go-imports)
- picom
- plan9port (for page and acme)
- shell-color-scripts (for colorscript in bashrc)
- ttf-twemoji [available in the AUR](https://aur.archlinux.org/packages/ttf-twemoji) (for emojis)
- ttf-jetbrains-mono-nerd (for nerd fonts)
- flameshot (for screenshots)


These can all be found in the core ArchLinux repositories. Nothing is guarenteed about other distributions.

### Custom applications:
- [Personal fork](https://gitlab.com/HostGrady/dmenu-fork) of dmenu (this includes non standard customizations)
- [Personal fork](https://gitlab.com/HostGrady/dwm-fork) of dwm (this could be any dwm in theory but it's untested so your fault if you break shit)

These probably won't be in your distribution, you will have to compile them manually using the build instructions provided or by using the standard tooling.

## TODO:
- [ ] Script to autoinstall all packages I use on ArchLinux
- [ ] Script to do the same for other distros if I switch (Void, Gentoo, Slackware, etc)
