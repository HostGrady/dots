#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# setting the path to allow scripts to be run
PATH="$PATH:$HOME/.local/bin:$HOME/.bin:$HOME/.local/bin/acme"

# for vim fzf plugin:
export FZF_DEFAULT_OPTS="--bind=enter:replace-query+print-query"
export FZF_DEFAULT_COMMAND='find . -type f | grep -viE "Steam|mozilla|go/pkg/|\.cache|\.git/|thunderbird|git/contrib/sites|Wallpaper|\.ghidra|\.java|docs|\.ssh|\.vim|Downloads|builds|\.config/chromium|\.pki|dmsounds|unity3d|projects/go/others|cookie|tmp|.*\.(png|jpg|jpeg|webp|ff|gz|tar|tgz|kdbx|mp3|mp4|wav|log|old|o|webm)$"'

# for less history
export LESSHISTFILE=-

# EDITOR
export EDITOR=vim

# aliases 

## adding pretty colours/formatting:
alias grep='grep --color=auto'
alias ls='exa -lahHg --git --group-directories-first --color=auto --icons'
alias diff='diff -u --color=auto'

## adding safety/confirmation:
alias rm='rm -iv'
alias cp='cp -iv'
alias mv='mv -iv'
alias yay='syay'

## for old habits
alias sxiv='nsxiv'

## acme stuff
alias acme='amacme'

## navigation stuff
alias gobk='cd "$OLDPWD"' # go back

git_personal="$HOME/git/personal/"
git_contrib="$HOME/git/contrib/"

alias gogit='cd $git_personal'
alias godwm='cd $git_personal/dwm-fork'
alias godmenu='cd $git_personal/dmenu-fork'
alias godots='cd $git_personal/dots'
alias godmscripts='cd $git_contrib/dmscripts'
alias lbin='cd $HOME/.local/bin'
alias lshare='cd $HOME/.local/share'
alias cfg='cd $HOME/.config'

## dev stuff
alias dmfmt='shfmt -bn -l -i=4 -ln=bash -w .' # for dmscripts

# functions

## dev stuff
developer_mode_dir="$HOME/.local/share/developer-modes"

## mirrors
update_mirrors() {
	local yn
	local tmp_mirror_file
	tmp_mirror_file="$(mktemp "/tmp/update_mirrors_mirrorlist_XXXXXXXXXX")"
	read -p "Make a backup? " yn
	if [ "$yn" = "y" ]; then
		rm -f "$HOME/mirrors"
		cp -vf "/etc/pacman.d/mirrorlist" "$HOME/mirrors"
	fi
	echo "⏰ Updating mirrors, this may take some time ⏰"
	notify-send "bash: update_mirrors" "⏰ Updating mirrors, this may take some time ⏰"
	reflector -c canada,us -p https -a 12 -f 10 > "$tmp_mirror_file" 
	sudo cp -vf "$tmp_mirror_file" "/etc/pacman.d/mirrorlist"
	echo "📦 All done! Now doing an update with pacman 📦"
	yay -Syyu
}


# prompt
# I used this site to generate the code: https://ezprompt.net/
nonzero_return() {
	RETVAL=$?
	[ "$RETVAL" -ne 0 ] && echo "$RETVAL "
}

# get current branch in git repo
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`parse_git_dirty`
		echo " [${BRANCH}${STAT}]"
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

export PS1="\[\e[31m\]\`nonzero_return\`\[\e[m\]\[\e[31m\][\[\e[m\]\[\e[33m\]\u\[\e[m\]\[\e[32m\]@\[\e[m\]\[\e[34m\]\h\[\e[m\] \[\e[35m\]\w\[\e[m\]\[\e[31m\]]\[\e[m\]\\$\[\e[36m\]\`parse_git_branch\`\[\e[m\] "


# colorscript
colorscript -r
